package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    boolean currentRound = true;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true) {
            if (currentRound) {
                System.out.println("Let's play round " + roundCounter);
                currentRound = false;
            }
            String userChoice = readInput("Your choice (Rock/Paper/Scissors)?");

            if (!rpsChoices.contains(userChoice)) {
                System.out.println("I do not understand " + userChoice + ". Could you try again?");
                continue;
            }

            String computerChoice = computerChoice();

            getWinner(userChoice, computerChoice);
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            String continuePlaying = readInput("Do you wish to continue playing? (y/n)?");
            if (continuePlaying.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            } else {
                roundCounter++;
                currentRound = true;
            }
        }
    }

    /**
     * Make computer choice
     */
    public String computerChoice() {
        int randomIndex = (int)(Math.random() * rpsChoices.size());
        return rpsChoices.get(randomIndex);
    }

    /**
     * Get winner
     */
    public void getWinner(String userChoice, String computerChoice) {
        if (userChoice.equals(computerChoice)) {
            System.out.println("Human chose " + userChoice + " computer chose " + computerChoice + ". It's a tie!");
        } else if (userChoice.equals("rock")) {
            if (computerChoice.equals("paper")) {
                System.out.println("Human chose " + userChoice + " computer chose " + computerChoice + ". Computer wins!");
                computerScore++;
            } else {
                System.out.println("Human chose " + userChoice + " computer chose " + computerChoice + ". Human wins!");
                humanScore++;
            }
        } else if (userChoice.equals("paper")) {
            if (computerChoice.equals("scissors")) {
                System.out.println("Human chose " + userChoice + " computer chose " + computerChoice + ". Computer wins!");
                computerScore++;
            } else {
                System.out.println("Human chose " + userChoice + " computer chose " + computerChoice + ". Human wins!");
                humanScore++;
            }
        } else if (userChoice.equals("scissors")) {
            if (computerChoice.equals("rock")) {
                System.out.println("Human chose " + userChoice + " computer chose " + computerChoice + ". Computer wins!");
                computerScore++;
            } else {
                System.out.println("Human chose " + userChoice + " computer chose " + computerChoice + ". Human wins!");
                humanScore++;
            }
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
